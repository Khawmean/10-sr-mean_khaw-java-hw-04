package com.company;

import java.util.ArrayList;
import java.util.List;

public class MyList<E> {
    private List<E> list = new ArrayList<>();
    public void addItem(E value){
        if(list.contains(value)){
            throw new ExceptionPlus();
        }
        list.add(value);
    }
    public E getItem(int index){
        return list.get(index);
    }
}
